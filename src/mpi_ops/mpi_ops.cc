#include "mpi_ops.h"

using namespace std;

#ifdef KARMA_MPI
    using namespace MPI;
#endif

namespace KARMA {
   
int MPI_Ops::Rank(void) {
#ifdef KARMA_MPI
    return COMM_WORLD.Get_rank();
#else 
    return 0
#endif
}

int MPI_Ops::Size(void) {
#ifdef KARMA_MPI
    return COMM_WORLD.Get_size();
#else
    return 1
#endif
}

} // end namespace KARMA