#ifndef KARMA_UTILS_MPI_OPS_H
#define KARMA_UTILS_MPI_OPS_H

#ifdef KARMA_MPI
    #include "mpi.h"
#endif

namespace KARMA {
    
class MPI_Ops {

    public:
        static int Rank (void);
        static int Size (void);
    
};

} // end namespace KARMA

#endif