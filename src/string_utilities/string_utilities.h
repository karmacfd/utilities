#ifndef KARMA_UTILS_STRING_H
#define KARMA_UTILS_STRING_H

#include <string>
#include <vector>
#include <sstream>

namespace KARMA {

class StringUtils {
public:
    static void StripComments   (std::string& str);
    static void StripWhiteSpaces(std::string& str);
    static void StripEmptyLines (std::string& str);
    static std::string ExtractInBetween(std::string& str,std::string& begin_tag,std::string &end_tag);
    static std::vector<std::string> Explode(std::string& str, char delim);
    static std::string int2str(const int &number);    
    static std::vector<int>    str2ivec (std::string& str);
    static std::vector<double> str2dvec (std::string& str);
};

} // end namespace KARMA

#endif
