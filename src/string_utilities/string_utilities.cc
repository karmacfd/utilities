#include "string_utilities.h"
#include <locale>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

namespace KARMA {

void StringUtils::StripComments(string& str) {
    // This doesn't remove the lines
    // If the comment is in it's own line, this will leave an empty line
    size_t beg=0;
    while (beg!=string::npos) {
        beg = str.find("#");
        if (beg!=string::npos) str.erase(beg,str.find("\n",beg)-beg);
    }
    return;    
}

void StringUtils::StripWhiteSpaces(string& str) {
    // This doesn't remove line breaks
    string whitespaces(" \t\f\v\r");                                          
    size_t beg=0;                                                             
    while (beg!=string::npos) {                                               
        beg=str.find_first_of(whitespaces);                                  
        if (beg!=string::npos) str.erase(beg,1);                           
    } 
    return;
}

void StringUtils::StripEmptyLines(string& str) {
    size_t beg=0;                                                             
    // If the first line is empty, remove it
    if (str.substr(0,1)=="\n") str.erase(0,1);
    while (beg!=string::npos) {                                               
        beg=str.find("\n\n");                                  
        if (beg!=string::npos) str.erase(beg,1);                           
    }     
    return;
}    
    
string StringUtils::ExtractInBetween(string& str,string& begin_tag,string &end_tag) {
    size_t begin_pos = str.find(begin_tag);
    if (begin_pos==string::npos) return "";
    size_t end_pos   = str.find(end_tag,begin_pos);
    if (end_pos==string::npos) return "";
    // Shift begin_pos by the tag length
    begin_pos += begin_tag.length();
    return str.substr(begin_pos,end_pos-begin_pos);
}
string StringUtils::int2str(const int &number) {
    std::stringstream dummy;
    dummy << number;
    //return dummy as string
    return dummy.str();
}

vector<string> StringUtils::Explode(string &str, char delim) {
    vector<string> result;
    istringstream iss(str);

    for (string token; getline(iss, token, delim); ) {
        result.push_back(move(token));
    }
    return result;
}

std::vector<int> StringUtils::str2ivec(string &str) {
    string begin_tag = "[";
    string end_tag   = "]";
    string str_in = ExtractInBetween(str,begin_tag,end_tag);
    vector<string> str_exp = Explode(str_in,',');
    vector<int> result(str_exp.size());
    for (int i=0;i<str_exp.size();++i) {
        result[i] = stoi(str_exp[i]);
    }
    return result;
}

std::vector<double> StringUtils::str2dvec(string &str) {
    string begin_tag = "[";
    string end_tag   = "]";
    string str_in = ExtractInBetween(str,begin_tag,end_tag);
    vector<string> str_exp = Explode(str_in,',');
    vector<double> result(str_exp.size());
    for (int i=0;i<str_exp.size();++i) {
        result[i] = stod(str_exp[i]);
    }
    return result;
}
    
} // end if namespace KARMA
