#include "vec3d.h"

using namespace std;

namespace KARMA {

// Constructors
Vec3D::Vec3D () {

}

Vec3D::Vec3D (const double &a) {
    comp[0]=a;
    comp[1]=a;
    comp[2]=a;
}

Vec3D::Vec3D (const double &x, const double &y, const double &z) {
    comp[0]=x;
    comp[1]=y;
    comp[2]=z;
}

// Copy constructor
Vec3D::Vec3D(const Vec3D &right) {
    memcpy(comp,right.comp,sizeof(comp));
}

// Assignment
Vec3D &Vec3D::operator= (const Vec3D &right) {
    memcpy(comp,right.comp,sizeof(comp));
    return *this;
}

double Vec3D::dot(const Vec3D &right) const {
    return (comp[0]*right.comp[0]+right.comp[1]*comp[1]+right.comp[2]*comp[2]);
}

double Vec3D::length(void) const {
    return sqrt((*this).dot(*this));
}

Vec3D Vec3D::cross(const Vec3D &right) const {
    return Vec3D(
            comp[1]*right.comp[2]-comp[2]*right.comp[1],
            -comp[0]*right.comp[2]+comp[2]*right.comp[0],
            comp[0]*right.comp[1]-comp[1]*right.comp[0]
            );
}

Vec3D Vec3D::norm(void) const {
    return (*this)/(*this).length();
}

Vec3D &Vec3D::operator= (const double &right) {
    comp[0]=right;
    comp[1]=right;
    comp[2]=right;
    return *this;
}

Vec3D &Vec3D::operator= (const std::vector<double> &right) {
    comp[0]=right[0];
    comp[1]=right[1];
    comp[2]=right[2];
    return *this;
}

Vec3D &Vec3D::operator*= (const double &right) {
    comp[0]*=right;
    comp[1]*=right;
    comp[2]*=right;
    return *this;
}

Vec3D Vec3D::operator*(const double &right) const {
    return Vec3D(comp[0]*right,comp[1]*right,comp[2]*right);	
}

Vec3D &Vec3D::operator/= (const double &right) {
    comp[0]/=right;
    comp[1]/=right;
    comp[2]/=right;
    return *this;
}

Vec3D Vec3D::operator/ (const double &right) const {
    return Vec3D (comp[0]/right,comp[1]/right,comp[2]/right);
}

Vec3D &Vec3D::operator+= (const double &right) {
    comp[0]+=right;
    comp[1]+=right;
    comp[2]+=right;
    return *this;
}

Vec3D &Vec3D::operator+= (const Vec3D &right) {
    comp[0]+=right.comp[0];
    comp[1]+=right.comp[1];
    comp[2]+=right.comp[2];
    return *this;
}

Vec3D Vec3D::operator+ (const double &right) const {
    return Vec3D(comp[0]+right,comp[1]+right,comp[2]+right);
}

Vec3D &Vec3D::operator-= (const double &right) {
    comp[0]-=right;
    comp[1]-=right;
    comp[2]-=right;
    return *this;
}

Vec3D &Vec3D::operator-= (const Vec3D &right) {
    comp[0]-=right.comp[0];
    comp[1]-=right.comp[1];
    comp[2]-=right.comp[2];
    return *this;
}

Vec3D Vec3D::operator- (const double &right) const {
    return Vec3D(comp[0]-right,comp[1]-right,comp[2]-right);
}

Vec3D operator*(const double &left, const Vec3D &right) {
    return Vec3D(left*right.comp[0],left*right.comp[1],left*right.comp[2]);
}

Vec3D operator+ (const double &left, const Vec3D &right) {
    return Vec3D(left+right.comp[0],left+right.comp[1],left+right.comp[2]);
}

Vec3D operator+ (const Vec3D &left, const Vec3D &right) {
    return Vec3D(left.comp[0]+right.comp[0],left.comp[1]+right.comp[1],left.comp[2]+right.comp[2]);
}

Vec3D operator- (const double &left, const Vec3D &right) {
    return Vec3D(left-right.comp[0],left-right.comp[1],left-right.comp[2]);
}

Vec3D operator- (const Vec3D &left, const Vec3D &right) {
    return Vec3D(left.comp[0]-right.comp[0],left.comp[1]-right.comp[1],left.comp[2]-right.comp[2]);
}

bool Vec3D::operator== (const Vec3D &right) const {
    return (comp[0]==right.comp[0] && comp[1]==right.comp[1] && comp[2]==right.comp[2]);
}

bool Vec3D::operator!= (const Vec3D &right) const {
    return (comp[0]!=right.comp[0] || comp[1]!=right.comp[1] || comp[2]!=right.comp[2]);
}

double &Vec3D::operator[] (const int &i) {
    return comp[i];
}

ostream &operator<< (ostream &output,const Vec3D &right) {
    output << "[" << right.comp[0] << "," << right.comp[1] << "," << right.comp[2] << "]";
    return output;
}

bool Vec3D::withinBox(const Vec3D &corner_1,const Vec3D &corner_2) const {
    for (int i=0;i<3;++i) {
        if (corner_1.comp[i]>corner_2.comp[i]) {
            if (comp[i]>corner_1.comp[i]) return false;
            if (comp[i]<corner_2.comp[i]) return false;
        } else {
            if (comp[i]<corner_1.comp[i]) return false;
            if (comp[i]>corner_2.comp[i]) return false;
        }
    }
    return true;
}

bool Vec3D::withinCylinder(const Vec3D &center,const double &radius,const Vec3D &axisDirection,const double &height) const {
    Vec3D onAxis=(*this-center).dot(axisDirection)*axisDirection;
    if (onAxis.length()>0.5*height) return false;

    Vec3D offAxis=(*this-center)-onAxis;
    if (offAxis.length()>radius) return false;

    return true;
}

bool Vec3D::withinSphere(const Vec3D &center,const double &radius) const {
    return ((*this-center).length()<=radius);
}


} // end namespace KARMA