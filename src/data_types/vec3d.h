#ifndef KARMA_UTILS_VEC3D_H
#define KARMA_UTILS_VEC3D_H

#include "screen.h"
#include <vector>
#include <math.h>

namespace KARMA {
    
class Vec3D {                                                                   
    protected:                                                                  
        double comp[3];                                                         
    public:                                                                     
        // Constructors                                                         
        Vec3D ();                                                               
        Vec3D (const double &a);                                                
        Vec3D (const double &x, const double &y, const double &z);              
        // Copy constructor                                                     
        Vec3D(const Vec3D &right);                                              

        // Assignment                                                           
        Vec3D &operator= (const Vec3D &);                                       
        Vec3D &operator= (const double &);                                      
        Vec3D &operator= (const std::vector<double> &);                         
        Vec3D &operator*= (const double &);                                     
        Vec3D &operator/= (const double &);                                     
        Vec3D &operator+= (const double &);                                     
        Vec3D &operator+= (const Vec3D &);                                      
        Vec3D &operator-= (const double &);                                     
        Vec3D &operator-= (const Vec3D &);                                      
        double dot (const Vec3D &right) const;                                  
        Vec3D cross(const Vec3D &right) const;                                  
        Vec3D norm(void) const;                                                 
        double length(void) const;                                              
        Vec3D operator*(const double &) const;                                  
        Vec3D operator/ (const double &) const;                                 
        Vec3D operator+ (const double &) const;                                 
        Vec3D operator- (const double &) const;                                 
        bool operator== (const Vec3D &) const;                                  
        bool operator!= (const Vec3D &) const;                                  
        double &operator[] (const int &);                                       
        bool withinBox(const Vec3D &corner_1,const Vec3D &corner_2) const;      
        bool withinCylinder(const Vec3D &center,const double &radius,const Vec3D &axisDirection,const double &height) const;
        bool withinSphere(const Vec3D &center,const double &radius) const;      
                                                                                
        friend Vec3D operator* (const double &left, const Vec3D &right);        
        friend Vec3D operator+ (const double &left, const Vec3D &right);        
        friend Vec3D operator- (const double &left, const Vec3D &right);        
        friend Vec3D operator+ (const Vec3D &left, const Vec3D &right);         
        friend Vec3D operator- (const Vec3D &left, const Vec3D &right);         
        friend std::ostream &operator<< (std::ostream &output,const Vec3D &right);
};
    
} // end namespace KARMA

#endif