#ifndef KARMA_UTILS_OPTIONS_PARSER_H
#define KARMA_UTILS_OPTIONS_PARSER_H

#include <string>
#include <map>
#include <vector>
#include "file_io.h"
#include "string_utilities.h"

#define ENTRY_BOOL    1
#define ENTRY_INT     2
#define ENTRY_DOUBLE  3
#define ENTRY_STRING  4

namespace KARMA {

class OptionEntry {
protected:
    std::string value_;
    bool is_found_;
public:
    OptionEntry (void) {value_=""; is_found_=false;}
    OptionEntry (std::string value,bool is_found=true) {value_=value;is_found_= (value_!="");}
    operator const std::string &()  {return value_;}
    operator int ()                 {return stoi(value_);}
    operator double ()              {return stod(value_);}
    operator bool ()                {return value_=="true";}
    operator std::vector<int> ()    {return StringUtils::str2ivec(value_);}
    operator std::vector<double> () {return StringUtils::str2dvec(value_);}
};

class OptionsSection {
protected:
    std::string name_;
    int level_;
    bool is_found_;
    OptionsSection *parent_;
    std::string content_;
    std::map<std::string,OptionsSection> children_;
    std::map<std::string,std::string> entry_string_;
    
    std::map<std::string,OptionEntry> entries_;
    
    void CreateChildSection(const std::string &name);
    
public:
    OptionsSection&     Section(const std::string &name);
    bool                IsFound(void);
    OptionEntry         Get(const std::string &name);
};

class OptionsParser: public OptionsSection {
private:
    std::string file_name_;

public:
    // Contructor
    OptionsParser();
    int SetFile(const std::string &file_name);

};

} // end namespace KARMA

#endif
