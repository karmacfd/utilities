#include "options_parser.h"

using namespace std;

namespace KARMA {
    
OptionsParser::OptionsParser(void) {
    name_    = "_ROOT_";
    level_   = 0;
    content_ = "";
    return;
}

int OptionsParser::SetFile(const string &file_name) {
    file_name_ = file_name;
    content_   = File_IO::FiletoString(file_name);
    StringUtils::StripWhiteSpaces(content_);
    StringUtils::StripComments(content_);
    StringUtils::StripEmptyLines(content_);
    return 1;
}

void OptionsSection::CreateChildSection(const string &name) {
    OptionsSection new_section;
    new_section.name_   = name;
    new_section.level_  = level_+1;
    new_section.parent_ = this;
    new_section.is_found_ = false;
        
    string sec_begin_tag = "";
    string sec_end_tag   = "";
    for (int i=0;i<new_section.level_;++i) {
        sec_begin_tag += "[";
        sec_end_tag   += "[";
    }
    sec_begin_tag += name;
    sec_end_tag   += "/"+name;
    for (int i=0;i<new_section.level_;++i) {
        sec_begin_tag += "]";
        sec_end_tag   += "]";
    }   
    
    new_section.content_ = StringUtils::ExtractInBetween(content_,sec_begin_tag,sec_end_tag); 
    if (new_section.content_ != "") {
        new_section.is_found_ = true;
    }
    StringUtils::StripEmptyLines(new_section.content_);
    children_.insert(pair<string,OptionsSection>(name,new_section));    
    return;
}

OptionsSection& OptionsSection::Section(const string &name) {
    // Check if the section is already created
    if (children_.find(name)==children_.end()) CreateChildSection(name);
    return children_[name];
}

bool OptionsSection::IsFound(void) {
    return is_found_;
}

OptionEntry OptionsSection::Get(const string &name) {
    // Check if the option is already parsed
    map<string,OptionEntry>::iterator it = entries_.find(name);
    if (it==entries_.end()) {
        // New entry
        string begin_tag = name+"=";
        string end_tag   = "\n";
        string value     = StringUtils::ExtractInBetween(content_,begin_tag,end_tag);
        OptionEntry new_entry(value);
        entries_.insert(pair<string,OptionEntry>(name,new_entry));
        return new_entry;
    } else {
        return it->second;
    }
}
    
} // end namespace KARMA